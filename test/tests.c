/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 Alessio Chiapperini.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../src/chip8.h"

static int count_pass;
static int count_fail;
static uint8_t fontset[FONTSIZ] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0,	/* 0 */
	0x20, 0x60, 0x20, 0x20, 0x70,	/* 1 */
	0xF0, 0x10, 0xF0, 0x80, 0xF0,	/* 2 */
	0xF0, 0x10, 0xF0, 0x10, 0xF0,	/* 3 */
	0x90, 0x90, 0xF0, 0x10, 0x10,	/* 4 */
	0xF0, 0x80, 0xF0, 0x10, 0xF0,	/* 5 */
	0xF0, 0x80, 0xF0, 0x90, 0xF0,	/* 6 */
	0xF0, 0x10, 0x20, 0x40, 0x40,	/* 7 */
	0xF0, 0x90, 0xF0, 0x90, 0xF0,	/* 8 */
	0xF0, 0x90, 0xF0, 0x10, 0xF0,	/* 9 */
	0xF0, 0x90, 0xF0, 0x90, 0x90,	/* A */
	0xE0, 0x90, 0xE0, 0x90, 0xE0,	/* B */
	0xF0, 0x80, 0x80, 0x80, 0xF0,	/* C */
	0xE0, 0x90, 0x90, 0x90, 0xE0,	/* D */
	0xF0, 0x80, 0xF0, 0x80, 0xF0,	/* E */
	0xF0, 0x80, 0xF0, 0x80, 0x80	/* F */
};

#define TEST(exp, test_name) \
	do { \
		(void)printf("[ %-8s ] " test_name "\n", "RUN"); \
		if (exp) { \
			(void)printf("[ %8s ] " test_name "\n", "OK"); \
			count_pass++; \
		} else { \
			(void)printf("[ %8s ] " test_name "\n", "NOK"); \
			count_fail++; \
		} \
	} while (0)

static void
setup(struct chip8 *c)
{
	chip8_init(c);
	srand((unsigned int) time(0));
	printf("[----------] Test environment set-up.\n");
}

static void
teardown(void)
{
	printf("[----------] Test environment teardown.\n");
}

static void
putopcode(uint16_t opcode, struct chip8 *c, size_t pos)
{
	c->memory[pos] = opcode >> 8;
	c->memory[pos + 1] = opcode & 0xFF;
}

int
main(void)
{
	struct chip8 c;
	int test;

	setup(&c);
	(void)printf("[==========] Running test cases.\n");

	test = 0;
	/* PC must be set to 0x200 */
	{
		test++;
		TEST(c.pc == 0x200, "PC set to 0x200");
	}

	/* Font is loaded into memory */
	{
		int isloaded = 1;
		test++;

		for (size_t i = 0; i < FONTSIZ && isloaded != 0; i++) {
			if (c.memory[FONT_STARTADDR + i] != fontset[i]) {
				isloaded = 0;
			}
		}
		TEST(isloaded == 1, "Font is loaded");
	}

	/* 00E0: display should be cleared */
	{
		int isclear = 1;
		test++;

		putopcode(0x00E0, &c, c.pc);
		chip8_step(&c);
		for (size_t px = 0; px < WIDTH * HEIGHT && isclear != 0; px++) {
			if (c.display[px] != 0) {
				isclear = 0;
			}
		}
		TEST(isclear == 1, "00E0: display cleared");
	}

	/* 00EE: return from subroutine */
	{
		uint8_t osp = ++c.sp;
		test++;

		putopcode(0x00EE, &c, c.pc);
		chip8_step(&c);
		TEST((--osp == c.sp) && (c.pc == c.stack[c.sp]),
		    "00EE: RET from subroutine");
	}

	/* 1123: jmp to location 0x123, pc = 0x123 */
	{
		test++;

		putopcode(0x1123, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 0x123, "1123: JMP to location 0x123");
	}

	/* 2456: call subroutine at addr 0x456 */
	{
		test++;
		c.pc = 0x55;
		c.sp = 0;

		putopcode(0x2456, &c, c.pc);
		chip8_step(&c);
		TEST((c.stack[0] == 0x57) && (c.sp == 1) &&
		    (c.pc == 0x456), "2456: call subroutine at addr 0x456");
	}

	/* 3879: skip next instruction if V8 = 0x79 */
	{
		test++;
		c.registers[8] = 0x79;
		c.pc = 0;

		putopcode(0x3879, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 4, "3879: skip next instruction, V8 = 0x79");
	}

	/* 4123: skip next instruction if V1 != 0x23 */
	{
		test++;
		c.registers[1] = 0x45;
		c.pc = 0;

		putopcode(0x4123, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 4, "4123: skip next instruction, V1 != 0x23");
	}

	/* 5210: skip next instruction if V2 = V1 */
	{
		test++;
		c.registers[2] = 0x69;
		c.registers[1] = c.registers[2];
		c.pc = 0;

		putopcode(0x5210, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 4, "5210: skip next instruction, V2 = V1");
	}

	/* 6456: set V4 = 0x56 */
	{
		test++;

		putopcode(0x6456, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[4] == 0x56, "6456: set V4 = 0x56");
	}

	/* 7789: set V7 = V7 + 0x89 */
	{
		test++;
		c.registers[7] = 0x0;

		putopcode(0x7789, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[7] == 0x89, "7789: set V7 = 0x89");
	}

	/* 8980: set V9 = V8 */
	{
		test++;
		c.registers[8] = 0x98;

		putopcode(0x8980, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == c.registers[8], "8980: set V9 = V8");
	}

	/* 8981: set V9 = V9 OR V8 */
	{
		test++;
		c.registers[9] = 0x32;
		c.registers[8] = 0x58;

		putopcode(0x8981, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0x7a, "8981: set V9 = V9 OR V8");
	}

	/* 8982: set V9 = V9 AND V8 */
	{
		test++;
		c.registers[9] = 0x32;
		c.registers[8] = 0x58;

		putopcode(0x8982, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0x10, "8982: set V9 = V9 AND V8");
	}

	/* 8983: set V9 = V9 XOR V8 */
	{
		test++;
		c.registers[9] = 0x32;
		c.registers[8] = 0x58;

		putopcode(0x8983, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0x6a, "8983: set V9 = V9 XOR V8");
	}

	/* 8984: set V9 = V9 + V8, VF = carry */
	{
		test++;
		c.registers[9] = 0x32;
		c.registers[8] = 0xFA;

		putopcode(0x8984, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0x2c && c.registers[0xF] == 1,
		    "8984: set V9 = V9 + V8, VF = carry");
	}

	/* 8985: set V9 = V9 - V8, VF = not borrow */
	{
		test++;
		c.registers[9] = 0xFA;
		c.registers[8] = 0x32;

		putopcode(0x8985, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0xc8 && c.registers[0xF] == 1,
		    "8985: set V9 = V9 - V8, VF = not borrow");
	}

	/* 8986: set V9 = V9 SHR 1 */
	{
		test++;
		c.registers[9] = 0xFA;

		putopcode(0x8986, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0x7D && c.registers[0xF] == 0,
		    "8986: set V9 = V9 SHR 1");
	}

	/* 8987: set V9 = V8 - V9, VF = not borrow */
	{
		test++;
		c.registers[9] = 0x32;
		c.registers[8] = 0xFA;

		putopcode(0x8987, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0xc8 && c.registers[0xF] == 1,
		    "8987: set V9 = V8 - V9, VF = not borrow");
	}

	/* 898E: V9 = V9 SHL 1 */
	{
		test++;
		c.registers[9] = 0x32;

		putopcode(0x898E, &c, c.pc);
		chip8_step(&c);
		TEST(c.registers[9] == 0x64 && c.registers[0xF] == 0,
		    "898E: set V9 = V9 SHL 1");
	}

	/* 9120: skip next instruction if V1 != V2 */
	{
		test++;
		c.registers[1] = 0x45;
		c.registers[2] = 0x32;
		c.pc = 0;

		putopcode(0x9120, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 4, "9120: skip next instruction, V1 != V2");
	}

	/* A123: set I to 0x123 */
	{
		test++;

		putopcode(0xA123, &c, c.pc);
		chip8_step(&c);
		TEST(c.index == 0x123, "A123: set I to 0x123");
	}

	/* B345: jump to location 0x345 + V0 */
	{
		test++;
		c.registers[0] = 0x12;
		c.pc = 0;

		putopcode(0xB345, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 0x357, "B345: jump to location 0x345 + 0x12 (V0)");
	}

	/* E69E: skip next instruction if key with the value of V6 is pressed */
	{
		test++;
		c.registers[6] = 1;
		c.keypad[c.registers[6]] = 1;
		c.pc = 0;

		putopcode(0xE69E, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 4, "E69E: skip next instruction, key with value of V6"
		    " is pressed");
	}

	/*
	 * E1A1: skip next instruction if key with the value of V1 is not
	 * pressed.
	 */
	{
		test++;
		c.registers[1] = 4;
		c.keypad[c.registers[1]] = 0;
		c.pc = 0;

		putopcode(0xE1A1, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 4, "E1A1: skip next instruction, "
		    "key with value of V1 is not pressed");
	}

	/* F507: set V5 = delay timer value */
	{
		test++;
		c.pc = 0;
		c.delay_timer = 4;

		putopcode(0xF507, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 2 && c.registers[5] == 4,
		    "F507: set V5 to delay timer value");
	}

	/* F20A: wait for a key press, store the value of the key in V2 */
	{
		test++;
		c.pc = 0;
		(void)memset(c.keypad, 0, sizeof c.keypad);
		c.keypad[0] = 1;

		putopcode(0xF20A, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 2 && c.registers[2] == 0,
		    "F20A: key 'x' is pressed, V2 = 0");
	}

	/* F515: set delay timer = V5 */
	{
		test++;
		c.pc = 0;
		c.registers[5] = 5;

		putopcode(0xF515, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 2 && c.delay_timer == 5,
		    "F515: set delay timer = V5");
	}

	/* F718: set sound timer = V7 */
	{
		test++;
		c.pc = 0;
		c.registers[7] = 7;

		putopcode(0xF718, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 2 && c.sound_timer == 7,
		    "F718: set sound timer = V7");
	}

	/* F01E: set I = I + V0 */
	{
		uint16_t oindex = c.index;

		test++;
		c.pc = 0;
		c.registers[0] = 1;

		putopcode(0xF01E, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 2 && c.index == oindex + 1,
		    "F01E: set I = I + V0");
	}

	/* F929: set I to location of sprite for digit V9 */
	{
		test++;
		c.pc = 0;
		c.registers[9] = 2;

		putopcode(0xF929, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 2 && c.index == 0x5A,
		    "F929: set I to location of sprite of '2'");
	}

	/*
	 * F333: store BCD representation of V3 in memory at locations I, I+1
	 * and I+2
	 */
	{
		test++;
		c.pc = 0;
		c.registers[3] = 231;

		putopcode(0xF333, &c, c.pc);
		chip8_step(&c);
		TEST(c.pc == 2 && c.memory[c.index + 2] == 1 &&
		    c.memory[c.index + 1] == 3 && c.memory[c.index] == 2,
		    "F333: store BCD representation of V3 in memory locations "
		    "I, I+1, I+2");
	}

	/* F255: store registers V0-V2 in memory starting from I */
	{
		int res;

		test++;
		c.pc = 0;
		c.registers[0] = 0;
		c.registers[1] = 1;
		c.registers[2] = 2;

		putopcode(0xF255, &c, c.pc);
		chip8_step(&c);

		res = 1;
		for (uint8_t r = 0; r <= 2 && res != 0; r++) {
			if (c.memory[c.index + r] != c.registers[r]) {
				res = 0;
			}
		}
		TEST(c.pc == 2 && res == 1,
		    "F255: store registers V0-V2 in memory starting from "
		    "location I");
	}

	/* F165: read registers V0-V1 in memory starting from I */
	{
		int res;

		test++;
		c.pc = 0;
		c.registers[1] = 10;
		c.registers[2] = 20;

		putopcode(0xF165, &c, c.pc);
		chip8_step(&c);

		res = 1;
		for (uint8_t r = 0; r <= 1 && res != 0; r++) {
			if (c.memory[c.index + r] != c.registers[r]) {
				res = 0;
			}
		}
		TEST(c.pc == 2 && res == 1,
		    "F165: read registers V0-V1 in memory starting from "
		    "location I");
	}

	(void)printf("[==========] %d test cases ran.\n", test);
	(void)printf("[  PASSED  ] %d tests.\n", count_pass);
	(void)printf("[  FAILED  ] %d tests.\n", count_fail);
	teardown();
	return (0);
}

