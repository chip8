.POSIX:

.PHONY: all check clean install uninstall

include config.mk

SRCS != echo src/*.c
OBJS = ${SRCS:.c=.o}

TESTDIR = test

all: ${PROG} tests

${PROG}: ${OBJS}
	${CC} ${LDFLAGS} -o $@ ${OBJS} ${LDLIBS}

tests: ${TESTDIR}/tests.o src/chip8.o
	${CC} ${LDFLAGS} -o $@ $? ${LDLIBS}

${PROG}.o: ${PROG}.c chip8.h

chip8.o: chip8.c chip8.h

${TESTDIR}/tests.o: ${TESTDIR}/tests.c src/chip8.h

check: tests
	./tests

clean:
	${RM} -f ${OBJS} ${TESTDIR}/tests.o ${PROG} tests

install: ${PROG} ${PROG}.1
	mkdir -p ${DESTDIR}${PREFIX}/bin
	install -m 755 ${PROG} ${DESTDIR}${PREFIX}/bin
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	gzip < ${PROG}.1 > ${DESTDIR}${MANPREFIX}/man1/${PROG}.1.gz

uninstall:
	${RM} -f ${DESTDIR}${PREFIX}/bin/${PROG}
	${RM} -f ${DESTDIR}${MANPREFIX}/man1/${PROG}.1.gz

.c.o:
	$(CC) -c $(CFLAGS) -o $@ $<
