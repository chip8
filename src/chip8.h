/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef CHIP8_H
#define CHIP8_H

#include <stdint.h>

enum {
	MEMSIZ		= 4096,
	STARTADDR	= 0x200,
	FONT_STARTADDR	= 0x50,
	REGISTERS	= 16,
	STACKSIZ	= 16,
	KEYS		= 16,
	WIDTH		= 64,
	HEIGHT		= 32,
	FONTSIZ		= 80,
};

struct chip8 {
	uint32_t	display[WIDTH * HEIGHT];
	uint16_t	stack[STACKSIZ];
	uint8_t		keypad[KEYS];
	uint8_t		memory[MEMSIZ];
	uint8_t		registers[REGISTERS];
	uint16_t	index;			    /* I register */
	uint16_t	pc;
	uint8_t		delay_timer;		    /* Delay timer register */
	uint8_t		sound_timer;		    /* Sound timer register */
	uint8_t		sp;
};

void	chip8_init(struct chip8 *c);
int	chip8_loadrom(const char *path, struct chip8 *c);
void	chip8_step(struct chip8 *c);
void	chip8_tick(struct chip8 *c);
void	chip8_info(struct chip8 *c);

#endif
