/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "chip8.h"

#define DEBUG 0

#define OPCODE_NNN(opcode) (opcode & 0xFFF)	/* Lowest 12 bits */
#define OPCODE_KK(opcode) (opcode & 0xFF)	/* Lowest 8 bits */
#define OPCODE_N(opcode) (opcode & 0xF)		/* Lowest 4 bits */
#define OPCODE_X(opcode) ((opcode >> 8) & 0xF)	/* Lower 4 bytes of high byte */
#define OPCODE_Y(opcode) ((opcode >> 4) & 0xF)	/* Upper 4 bytes of low byte */
#define OPCODE_P(opcode) (opcode >> 12)		/* Upper 4 bytes */

#define debug_print(fmt, ...)						\
    do { if (DEBUG) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__,	\
	__LINE__, __func__, __VA_ARGS__); } while (0)

typedef void opcode_handler(uint16_t opcode, struct chip8 *c);

static uint8_t fontset[FONTSIZ] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0,	/* 0 */
	0x20, 0x60, 0x20, 0x20, 0x70,	/* 1 */
	0xF0, 0x10, 0xF0, 0x80, 0xF0,	/* 2 */
	0xF0, 0x10, 0xF0, 0x10, 0xF0,	/* 3 */
	0x90, 0x90, 0xF0, 0x10, 0x10,	/* 4 */
	0xF0, 0x80, 0xF0, 0x10, 0xF0,	/* 5 */
	0xF0, 0x80, 0xF0, 0x90, 0xF0,	/* 6 */
	0xF0, 0x10, 0x20, 0x40, 0x40,	/* 7 */
	0xF0, 0x90, 0xF0, 0x90, 0xF0,	/* 8 */
	0xF0, 0x90, 0xF0, 0x10, 0xF0,	/* 9 */
	0xF0, 0x90, 0xF0, 0x90, 0x90,	/* A */
	0xE0, 0x90, 0xE0, 0x90, 0xE0,	/* B */
	0xF0, 0x80, 0x80, 0x80, 0xF0,	/* C */
	0xE0, 0x90, 0x90, 0x90, 0xE0,	/* D */
	0xF0, 0x80, 0xF0, 0x80, 0xF0,	/* E */
	0xF0, 0x80, 0xF0, 0x80, 0x80	/* F */
};

static void
nibble_0(uint16_t opcode, struct chip8 *c)
{
	int oerrno;

	if (opcode == 0x00E0) {
		/* 00E0 - CLS: Clear the display */
		(void)memset(c->display, 0, sizeof c->display);
	} else if (opcode == 0x00EE) {
		/* 00EE - RET: Return from a subroutine */
		if (c->sp == 0) {
			oerrno = errno;
			errno = EOVERFLOW;
			perror("RET");
			errno = oerrno;
			exit(1);
		}
		c->sp--;
		c->pc = c->stack[c->sp];
	}
}

static void
nibble_1(uint16_t opcode, struct chip8 *c)
{
	/* 1NNN - JP addr: Jump to location NNN */
	c->pc = OPCODE_NNN(opcode);
}

static void
nibble_2(uint16_t opcode, struct chip8 *c)
{
	int oerrno;

	if (c->sp >= 16) {
		oerrno = errno;
		errno = EOVERFLOW;
		perror("RET");
		errno = oerrno;
		exit(1);
	}
	c->stack[c->sp] = c->pc;
	c->sp++;
	/* 2NNN - CALL addr: Call subroutine at NNN */
	c->pc = OPCODE_NNN(opcode);
}

static void
nibble_3(uint16_t opcode, struct chip8 *c)
{
	/* 3xkk - SE Vx, byte: Skip next instruction if Vx = kk */
	if (c->registers[OPCODE_X(opcode)] == OPCODE_KK(opcode)) {
		c->pc += 2;
	}
}

static void
nibble_4(uint16_t opcode, struct chip8 *c)
{
	/* 4xkk - SNE Vx, byte: Skip next instruction if Vx != kk */
	if (c->registers[OPCODE_X(opcode)] != OPCODE_KK(opcode)) {
		c->pc += 2;
	}
}

static void
nibble_5(uint16_t opcode, struct chip8 *c)
{
	/* 5xy0 - SE Vx, Vy: Skip next instruction if Vx = Vy */
	if (c->registers[OPCODE_X(opcode)] == c->registers[OPCODE_Y(opcode)]) {
		c->pc += 2;
	}
}

static void
nibble_6(uint16_t opcode, struct chip8 *c)
{
	/* 6xkk - LD Vx, byte: Set Vx = kk */
	c->registers[OPCODE_X(opcode)] = OPCODE_KK(opcode);
}

static void
nibble_7(uint16_t opcode, struct chip8 *c)
{
	uint8_t x;

	x = OPCODE_X(opcode);
	/* 7xkk - ADD Vx, byte: Set Vx = Vx + kk */
	c->registers[x] += OPCODE_KK(opcode);
}

static void
nibble_8(uint16_t opcode, struct chip8 *c)
{
	uint16_t sum;
	uint8_t x, y;
	int oerrno;

	x = OPCODE_X(opcode);
	y = OPCODE_Y(opcode);
	switch(OPCODE_N(opcode)) {
	case 0:
		/* 8xy0 - LD Vx, Vy: Set Vx = Vy */
		c->registers[x] = c->registers[y];
		break;
	case 1:
		/* 8xy1 - OR Vx, Vy: Set Vx = Vx OR Vy */
		c->registers[x] |= c->registers[y];
		break;
	case 2:
		/* 8xy2 - AND Vx, Vy: Set Vx = Vx AND Vy */
		c->registers[x] &= c->registers[y];
		break;
	case 3:
		/* 8xy3 - XOR Vx, Vy: Set Vx = Vx XOR Vy */
		c->registers[x] ^= c->registers[y];
		break;
	case 4:
		/* 8xy4 - ADD Vx, Vy: Set Vx = Vx + Vy, set VF = carry */
		sum = c->registers[x] + c->registers[y];
		c->registers[0xF] = (sum > 255u) ? 1 : 0;
		c->registers[x] = sum & 0xFF;
		break;
	case 5:
		/* 8xy5 - SUB Vx, Vy: Set Vx = Vx - Vy, set VF = NOT borrow */
		c->registers[0xF] = c->registers[x] > c->registers[y];
		c->registers[x] -= c->registers[y];
		break;
	case 6:
		/* 8xy6 - SHR Vx {, Vy}: Set Vx = Vx SHR 1 */
		c->registers[0xF] = c->registers[x] & 1;
		c->registers[x] >>= 1;
		break;
	case 7:
		/* 8xy7 - SUBN Vx, Vy: Set Vx = Vy - Vx, set VF = NOT borrow */
		c->registers[0xF] = c->registers[y] > c->registers[x];
		c->registers[x] = c->registers[y] - c->registers[x];
		break;
	case 0xE:
		/* 8xyE - SHL Vx {, Vy}: Set Vx = Vx SHL 1 */
		c->registers[0xF]= (c->registers[x] & 0x80) >> 7;
		c->registers[x] <<= 1;
		break;
	default:
		/* UNREACHABLE */
		oerrno = errno;
		errno = EOPNOTSUPP;
		perror(0);
		errno = oerrno;
		exit(1);
	}
}

static void
nibble_9(uint16_t opcode, struct chip8 *c)
{
	/* 9xy0 - SNE Vx, Vy: Skip next instruction if Vx != Vy */
	if (c->registers[OPCODE_X(opcode)] != c->registers[OPCODE_Y(opcode)]) {
		c->pc += 2;
	}
}

static void
nibble_A(uint16_t opcode, struct chip8 *c)
{
	/* Annn - LD I, addr: Set I = nnn */
	c->index = OPCODE_NNN(opcode);
}

static void
nibble_B(uint16_t opcode, struct chip8 *c)
{
	/* Bnnn - JP V0, addr: Jump to location nnn + V0 */
	c->pc = c->registers[0] + OPCODE_NNN(opcode);
}

static void
nibble_C(uint16_t opcode, struct chip8 *c)
{
	/* Cxkk - RND Vx, byte: Set Vx = random byte AND kk */
	c->registers[OPCODE_X(opcode)] = (rand() % 256) & OPCODE_KK(opcode);
}

static void
nibble_D(uint16_t opcode, struct chip8 *c)
{
	/* Dxyn - DRW Vx, Vy, nibble: Display n-byte sprite starting at memory
	 * location I at (Vx, Vy), set VF = collision
	 */
	uint32_t *screenpx;
	uint8_t spriteb;
	uint8_t spritepx;
	uint8_t height;
	uint8_t x, y;
	uint8_t px, py;

	x = OPCODE_X(opcode);
	y = OPCODE_Y(opcode);
	height = OPCODE_N(opcode);
	px = c->registers[x] % WIDTH;
	py = c->registers[y] % HEIGHT;
	c->registers[0xF] = 0;

	for (uint8_t row = 0; row < height; row++) {
		spriteb = c->memory[c->index + row];

		for (uint8_t col = 0; col < 8; col++) {
			spritepx = spriteb & (0x80 >> col);
			screenpx = &c->display[(py + row) * WIDTH + (px + col)];

			if (spritepx != 0) {
				if (*screenpx == 0xFFFFFFFF) {
					c->registers[0xF] = 1;
				}

				*screenpx ^= 0xFFFFFFFF;
			}
		}
	}

}

static void
nibble_E(uint16_t opcode, struct chip8 *c)
{
	uint8_t key = c->registers[OPCODE_X(opcode)];
	if (OPCODE_KK(opcode) == 0x9E){
		/* Ex9E - SKP Vx: Skip next instruction if key with the value of
		 * Vx is pressed
		*/
		if (c->keypad[key] != 0) {
			c->pc += 2;
		}
	} else if (OPCODE_KK(opcode) == 0xA1) {
		/* ExA1 - SKNP Vx: Skip next instruction if key with the value
		 * of Vx is not pressed
		 */
		if (c->keypad[key] == 0) {
			c->pc += 2;
		}
	}
}

static void
nibble_F(uint16_t opcode, struct chip8 *c)
{
	int oerrno;
	uint8_t x;
	uint8_t rx;

	x = OPCODE_X(opcode);
	switch (OPCODE_KK(opcode)) {
	case 0x07:
		/* Fx07 - LD Vx, DT: Set Vx = delay timer value */
		c->registers[x] = c->delay_timer;
		break;
	case 0x0A:
		/* Fx0A - LD Vx, K: Wait for a key press, store the value of
		 * the key in Vx */
		if (c->keypad[0] != 0) {
			c->registers[x] = 0;
		} else if (c->keypad[1] != 0) {
			c->registers[x] = 1;
		} else if (c->keypad[2] != 0) {
			c->registers[x] = 2;
		} else if (c->keypad[3] != 0) {
			c->registers[x] = 3;
		} else if (c->keypad[4] != 0) {
			c->registers[x] = 4;
		} else if (c->keypad[5] != 0) {
			c->registers[x] = 5;
		} else if (c->keypad[6] != 0) {
			c->registers[x] = 6;
		} else if (c->keypad[7] != 0) {
			c->registers[x] = 7;
		} else if (c->keypad[8] != 0) {
			c->registers[x] = 8;
		} else if (c->keypad[9] != 0) {
			c->registers[x] = 9;
		} else if (c->keypad[10] != 0) {
			c->registers[x] = 10;
		} else if (c->keypad[11] != 0) {
			c->registers[x] = 11;
		} else if (c->keypad[12] != 0) {
			c->registers[x] = 12;
		} else if (c->keypad[13] != 0) {
			c->registers[x] = 13;
		} else if (c->keypad[14] != 0) {
			c->registers[x] = 14;
		} else if (c->keypad[15] != 0) {
			c->registers[x] = 15;
		} else {
			c->pc -= 2;
		}
		break;
	case 0x15:
		/* Fx15 - LD DT, Vx: Set delay timer = Vx */
		c->delay_timer = c->registers[x];
		break;
	case 0x18:
		/* Fx18 - LD ST, Vx: Set sound timer = Vx */
		c->sound_timer = c->registers[x];
		break;
	case 0x1E:
		/* Fx1E - ADD I, Vx: Set I = I + Vx */
		c->index += c->registers[x];
		break;
	case 0x29:
		/* Fx29 - LD F, Vx: Set I = location of sprite for digit Vx */
		c->index = FONT_STARTADDR + (5 * c->registers[x]);
		break;
	case 0x33:
		/* Fx33 - LD B, Vx: Store BCD representation of Vx in memory
		 * locations I, I+1, and I+2
		 */
		rx = c->registers[x];

		c->memory[c->index + 2] = rx % 10;
		rx /= 10;
		c->memory[c->index + 1] = rx % 10;
		rx /= 10;
		c->memory[c->index] = rx % 10;
		break;
	case 0x55:
		/* Fx55 - LD [I], Vx: Store registers V0 through Vx in memory
		 * starting at location I
		 */
		for (uint8_t i = 0; i <= x; i++) {
			c->memory[c->index + i] = c->registers[i];
		}
		break;
	case 0x65:
		/* Fx65 - LD Vx, [I]: Read registers V0 through Vx from memory
		 * starting at location I
		 */
		for (uint8_t i = 0; i <= x; i++) {
			 c->registers[i] = c->memory[c->index + i];
		}
		break;
	default:
		/* UNREACHABLE */
		oerrno = errno;
		errno = EOPNOTSUPP;
		perror(0);
		errno = oerrno;
		exit(1);
	}

}

static opcode_handler *nibble[16] = {
	&nibble_0, &nibble_1, &nibble_2, &nibble_3,
	&nibble_4, &nibble_5, &nibble_6, &nibble_7,
	&nibble_8, &nibble_9, &nibble_A, &nibble_B,
	&nibble_C, &nibble_D, &nibble_E, &nibble_F,
};

/*
 * Initializes CHIP-8's state.
 * Most CHIP-8's ROMs start at address 0x200.
 */
void
chip8_init(struct chip8 *c)
{
	if (c != 0) {
		*c = (struct chip8) {
			.pc = STARTADDR,
		};

		for (size_t i = 0; i < FONTSIZ; i++) {
			c->memory[FONT_STARTADDR + i] = fontset[i];
		}
	}
}

/*
 * Loads the provided ROM into the CHIP-8's memory.
 *
 * On success 0 is returned, 1 otherwise.
 */
int
chip8_loadrom(const char *path, struct chip8 *c)
{
	unsigned char buf[MEMSIZ - STARTADDR + 1] = { 0 };
	int ret;
	int fd;
	ssize_t size;

	if ((fd = open(path, O_RDONLY)) < 0) {
		perror(path);
		ret = 1;
		goto open_fail;
	}

	if ((size = read(fd, buf, MEMSIZ - STARTADDR + 1)) < 0) {
		perror(path);
		ret = 1;
		goto read_fail;
	}

	debug_print("%zd bytes read from %s\n", size, path);

	for (ssize_t i = 0; i < size; i++) {
		c->memory[STARTADDR + i] = buf[i];
	}

	ret = 0;
read_fail:
	close(fd);
open_fail:
	return (ret);
}

/*
 * Steps through an instuction by fetching, deconding and executing it.
 */
void
chip8_step(struct chip8 *c)
{
	uint16_t opcode;

	/* Fetch */
	opcode = (uint16_t) (c->memory[c->pc] << 8u) | c->memory[c->pc + 1];
	c->pc += 2;

	/* Decode and execute */
	debug_print("Executing opcode 0x%x.\n", opcode);
	nibble[OPCODE_P(opcode)](opcode, c);
}

/*
 * Updates the delay and sound timers if set.
 */
void
chip8_tick(struct chip8 *c)
{
	if (c->delay_timer > 0) {
		c->delay_timer--;
	}
	if (c->sound_timer > 0) {
		c->sound_timer--;
		if (c->sound_timer == 0) {
			printf("BEEP!\n");
		}
	}
}

/*
 * Prints debugging information on the state of the execution, such as: current
 * program counter, stack pointer, index register, content of the stack and
 * registers.
 */
void
chip8_info(struct chip8 *c)
{
	debug_print("PC: 0x%x\t\tSP: 0x%x\t\tI: 0x%x\n",
	    c->pc, c->sp, c->index);

	for (int i = STACKSIZ; i > 0; i--) {
		debug_print("%3d: 0x%x\n", i, c->stack[i]);
	}

	for (int i = 0; i < REGISTERS; i++) {
		debug_print("REG %3d: 0x%x\n", i, c->registers[i]);
	}
}
