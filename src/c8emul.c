/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <time.h>
#include <unistd.h>
#include <SDL.h>

#include "chip8.h"

static SDL_Window *window;
static SDL_Renderer *renderer;
static SDL_Texture *texture;

static void
usage(void)
{
	(void)fprintf(stderr,
	    "usage: c8emul [-s scaling_factor] [-d cycle_delay ] [rom_file]\n");
	exit(1);
}

static void
destroy_window(void)
{
	if (texture != 0) {
		SDL_DestroyTexture(texture);
		texture = 0;
	}

	if (renderer != 0) {
		SDL_DestroyRenderer(renderer);
		renderer = 0;
	}

	if (window != NULL) {
		SDL_DestroyWindow(window);
		window = 0;
	}
	SDL_Quit();
}

static int
init_window(int wwidth, int wheight, int twidth, int theight)
{
	int ret;

	ret = 0;
	window = 0;
	renderer = 0;
	texture = 0;

	if (SDL_Init(SDL_INIT_VIDEO)) {
		perror(SDL_GetError());
		ret = 1;
		goto init_error;
	}

	window = SDL_CreateWindow("CHIP-8 Emulator",
	    SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, wwidth, wheight,
	    SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
	    SDL_TEXTUREACCESS_STREAMING, twidth, theight);

	if (window == 0 || renderer == 0 || texture == 0) {
		ret = 1;
		destroy_window();
	}

init_error:
	return (ret);
}

static void
render(void const *buf, int pitch)
{
	SDL_UpdateTexture(texture, 0, buf, pitch);
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, 0, 0);
	SDL_RenderPresent(renderer);
}

static int
getinput(struct chip8 *c)
{
	int quit = 0;
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			quit = 1;
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
			case SDLK_ESCAPE:
				quit = 1;
				break;
			case SDLK_x:
				c->keypad[0] = 1;
				break;
			case SDLK_1:
				c->keypad[1] = 1;
				break;
			case SDLK_2:
				c->keypad[2] = 1;
				break;
			case SDLK_3:
				c->keypad[3] = 1;
				break;
			case SDLK_q:
				c->keypad[4] = 1;
				break;
			case SDLK_w:
				c->keypad[5] = 1;
				break;
			case SDLK_e:
				c->keypad[6] = 1;
				break;
			case SDLK_a:
				c->keypad[7] = 1;
				break;
			case SDLK_s:
				c->keypad[8] = 1;
				break;
			case SDLK_d:
				c->keypad[9] = 1;
				break;
			case SDLK_z:
				c->keypad[0xA] = 1;
				break;
			case SDLK_c:
				c->keypad[0xB] = 1;
				break;
			case SDLK_4:
				c->keypad[0xC] = 1;
				break;
			case SDLK_r:
				c->keypad[0xD] = 1;
				break;
			case SDLK_f:
				c->keypad[0xE] = 1;
				break;
			case SDLK_v:
				c->keypad[0xF] = 1;
				break;
			}
			break;
		case SDL_KEYUP:
			switch (event.key.keysym.sym) {
			case SDLK_x:
				c->keypad[0] = 0;
				break;
			case SDLK_1:
				c->keypad[1] = 0;
				break;
			case SDLK_2:
				c->keypad[2] = 0;
				break;
			case SDLK_3:
				c->keypad[3] = 0;
				break;
			case SDLK_q:
				c->keypad[4] = 0;
				break;
			case SDLK_w:
				c->keypad[5] = 0;
				break;
			case SDLK_e:
				c->keypad[6] = 0;
				break;
			case SDLK_a:
				c->keypad[7] = 0;
				break;
			case SDLK_s:
				c->keypad[8] = 0;
				break;
			case SDLK_d:
				c->keypad[9] = 0;
				break;
			case SDLK_z:
				c->keypad[0xA] = 0;
				break;
			case SDLK_c:
				c->keypad[0xB] = 0;
				break;
			case SDLK_4:
				c->keypad[0xC] = 0;
				break;
			case SDLK_r:
				c->keypad[0xD] = 0;
				break;
			case SDLK_f:
				c->keypad[0xE] = 0;
				break;
			case SDLK_v:
				c->keypad[0xF] = 0;
				break;
			}
			break;
		}
	}

	return (quit);
}

int
main(int argc, char *argv[])
{
	struct chip8 c;
	uint32_t oclock, clock;
	unsigned delay;
	int oerrno;
	int opt;
	int pitch;
	int quit;
	int scale_factor;
	char *def[] = { "-" };

	scale_factor = 1;
	delay = 3;
	while ((opt = getopt(argc, argv, "s:d:")) != -1) {
		switch (opt) {
		case 's':
			if (sscanf(optarg, "%d", &scale_factor) < 1) {
				oerrno = errno;
				errno = EINVAL;
				perror("c8emul");
				errno = oerrno;
				return (1);
			}
			break;
		case 'd':
			if (sscanf(optarg, "%u", &delay) < 1) {
				oerrno = errno;
				errno = EINVAL;
				perror("c8emul");
				errno = oerrno;
				return (1);
			}
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (argc == 0) {
		argc = 1;
		argv = def;
	}

	if (argc > 1) {
		errno = E2BIG;
		perror("c8emul");
		return (1);
	}

	if (init_window(WIDTH * scale_factor, HEIGHT * scale_factor,
	    WIDTH, HEIGHT) == 1) {
		return (1);
	}
	srand((unsigned int) time(0));

	chip8_init(&c);
	if (chip8_loadrom(argv[0], &c) == 1) {
		return (1);
	}

	pitch = sizeof(c.display[0]) * WIDTH;
	oclock = SDL_GetTicks();

	quit = 0;
	while (quit != 1) {
		quit = getinput(&c);
		clock = SDL_GetTicks();

		if ((clock - oclock) > delay) {
			oclock = clock;

			chip8_step(&c);
			chip8_tick(&c);
			render(c.display, pitch);
		}

		SDL_Delay(1);
	}

	return (0);
}
